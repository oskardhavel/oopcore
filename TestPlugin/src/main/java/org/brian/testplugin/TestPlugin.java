package org.brian.testplugin;

import org.apache.commons.math3.util.Pair;
import org.brian.core.events.SyncEvents;
import org.brian.core.message.LineContent;
import org.brian.core.message.MessageLine;
import org.brian.core.newCommand.CommandController;
import org.brian.core.newCommand.OOPCommand;
import org.brian.core.newCommand.arg.CmdArgument;
import org.brian.core.plugin.OOPPlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class TestPlugin extends OOPPlugin {

    @Override
    public void enable() {

        SyncEvents.listen(AsyncPlayerChatEvent.class, event -> {

            MessageLine messageLine = new MessageLine().append("&c");
            for (int i = 0; i < 13; i++) {

                if (i == 3)
                    messageLine.append("&b");
                else if (i == 6) {
                    messageLine.append("&a");
                }

                messageLine.append(new LineContent("TexS" + i + " "));

            }

            messageLine.send(event.getPlayer());

        }, this);

        OOPCommand oopCommand = new OOPCommand().
                label("testCommand").
                alias("tc").
                alias("tsc").
                description("Test Command!").
                subCommand(
                        new OOPCommand().
                                label("example").
                                description("The Example Sub Command").
                                argument(
                                        new CmdArgument().
                                                description("An Online Player").
                                                identity("player").
                                                required(true)
                                ).
                                argument(
                                        new CmdArgument().
                                                description("The Amount Of Hoppers").
                                                identity("amount")
                                ).
                                listen(cmd -> {

                                    cmd.sender().sendMessage("Sent");


                                })
                );

        CommandController commandController = new CommandController(this);
        commandController.register(oopCommand);

    }
}
