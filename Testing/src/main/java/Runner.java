import org.nustaq.serialization.FSTConfiguration;
import org.nustaq.serialization.FSTObjectInput;
import org.nustaq.serialization.FSTObjectOutput;
import java.util.*;

public class Runner {

    public static void main(String[] args) {

        FSTConfiguration fstConfig = FSTConfiguration.createDefaultConfiguration();
        try {

            FSTObjectOutput out = fstConfig.getObjectOutput();
            HashSet<TestObject> objects = new HashSet<>();

            for(int i = 0; i < 999; i++) {
                objects.add(new TestObject());
            }

            out.writeObject(objects, HashSet.class);
            String encoded = Base64.getEncoder().encodeToString(out.getBuffer());
            out.close();

            FSTObjectInput in = fstConfig.getObjectInput(Base64.getDecoder().decode(encoded));

            Set<TestObject> testObjectList = ((Set<TestObject>) in.readObject(HashSet.class));
            System.out.println(testObjectList.size());

        } catch (Exception ex){
            ex.printStackTrace();
        }

    }

}
