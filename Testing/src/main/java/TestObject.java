import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class TestObject implements Serializable {

    private int oof = ThreadLocalRandom.current().nextInt(105555);
    private Map<String, Object> data = new HashMap<>();

    public TestObject() {

        for(int i = 0; i < 20; i++) {

            data.put("i" + i, i);

        }

    }

    public int getOOF(){
        return oof;
    }
}
