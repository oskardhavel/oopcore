package org.brian.core.energiser;

import java.util.ArrayList;
import java.util.List;

public class RunnerThread extends Thread {

    private final Energiser energiser;
    private boolean shutdown = false;
    private List<ETask> tasks = new ArrayList<>();

    public RunnerThread(String name, Energiser energiser) {
        super(name);
        this.energiser = energiser;
        start();
    }

    public void assignTask(ETask task) {
        if (!tasks.contains(task)) {
            tasks.add(task);
        }
    }

    @Override
    public void run() {
        while (!shutdown) {

            try {
                if (tasks.isEmpty()) sleep(2);
                List<ETask> tasksCopy = new ArrayList<>(tasks);
                for (ETask task : tasksCopy) {
                    try {
                        task.taskConsumer().accept(task);
                    } catch (Exception ex) {
                        energiser.getHandler().getErrorManager().error(ex.getMessage(), ex, 3);
                    }
                    tasks.remove(task);
                }

            } catch (Exception ex) {
                energiser.getHandler().getErrorManager().error(ex.getMessage(), ex, 3);
            }

        }

        interrupt();

    }

    public boolean isShutdown() {
        return shutdown;
    }

    public void shutdown() {
        this.shutdown = true;
    }
}
