package org.brian.core.energiser;

import java.util.concurrent.TimeUnit;

public interface IScheduler {

    void schedule(ETask eTask);

   default void async(Runnable runnable, boolean pool) {}

    default void async(Runnable runnable) {
        async(runnable, false);
    }

    default ETask scheduleDelayed(Runnable runnable, long delay, TimeUnit unit) {

        ETask task = new ETask((t) -> runnable.run());
        task.delay(delay);
        schedule(task);
        return task;

    }

    default ETask scheduleDelayed(Runnable runnable, long delay) {
        return scheduleDelayed(runnable, delay, TimeUnit.MILLISECONDS);
    }

    default ETask scheduleRepeated(Runnable runnable, long delay, TimeUnit unit) {

        ETask task = new ETask((t) -> runnable.run());
        task.delay(unit.toMillis(delay));
        task.repeat(true);
        schedule(task);
        return task;
        
    }

    default ETask scheduleRepeated(Runnable runnable, long delay) {
        return scheduleRepeated(runnable, delay, TimeUnit.MILLISECONDS);
    }

}
