package org.brian.core.energiser;

import org.brian.core.OOPCore;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;

public class EnergiserFallBack implements IScheduler {

    private static EnergiserFallBack instance;
    private OOPCore oopCore;
    private List<Integer> taskIds = new ArrayList<>();

    public EnergiserFallBack(OOPCore core) throws IllegalAccessException {
        if (instance != null) throw new IllegalAccessException("Tried to reinitialize EnergiserFallBack.");
        instance = this;
        this.oopCore = core;
        oopCore.onDisable(() -> {
            shutdown();
            instance = null;
        });
    }

    public static EnergiserFallBack getInstance() {
        return instance;
    }

    @Override
    public void schedule(ETask eTask) {

        Runnable runnable = () -> eTask.taskConsumer().accept(eTask);
        eTask.bukkitMode(true);
        BukkitTask bukkitTask;
        if (eTask.repeat()) {
            bukkitTask = Bukkit.getScheduler().runTaskTimerAsynchronously(oopCore, runnable, 0, toTicks(eTask.delay()));
        } else if (eTask.delay() != -1) {
            bukkitTask = Bukkit.getScheduler().runTaskLaterAsynchronously(oopCore, runnable, toTicks(eTask.delay()));
        } else
            bukkitTask = Bukkit.getScheduler().runTaskAsynchronously(oopCore, runnable);

        eTask.id(bukkitTask.getTaskId());
    }

    public void sync(ETask eTask) {
        Bukkit.getScheduler().runTask(oopCore, () -> eTask.taskConsumer().accept(eTask));
    }

    public void shutdown() {
        taskIds.forEach(id -> Bukkit.getScheduler().cancelTask(id));
    }

    public long toTicks(long milis) {

        long ticks = Math.round(milis / 1000 * 20);
        return ticks;

    }

}
