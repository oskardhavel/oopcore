package org.brian.core.energiser;

import com.google.common.collect.HashBiMap;
import org.apache.commons.math3.util.Pair;
import org.brian.core.OOPCore;
import org.brian.core.utils.Helper;
import org.bukkit.Bukkit;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Handler;

public class Energiser implements IScheduler {

    private static HashBiMap<Pair<Class, Long>, Energiser> energiserMap = HashBiMap.create();
    private static AtomicLong idCounter = new AtomicLong(0);
    private static boolean allowedToInit = true;
    final Class<?> initializer;
    final long id;
    private final EnergiserScheduler scheduler;
    private final ExecutorService asyncPool;
    final private Handler handler;

    public Energiser(String name, Handler handler, Class<?> initializer) {

        if (!allowedToInit) {

            scheduler = null;
            asyncPool = null;
            this.initializer = initializer;
            this.handler = handler;
            this.id = idCounter.incrementAndGet();

        } else {

            this.initializer = initializer;
            this.id = idCounter.incrementAndGet();
            this.handler = handler;

            this.asyncPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() / 2, r -> new Thread(r, "Energiser-" + name + "-Async Thread"));
            this.scheduler = new EnergiserScheduler(name, this);
        }

        Pair<Class, Long> finder = new Pair<>(initializer, id);
        energiserMap.put(finder, this);

    }

    public static void shutdownAll() {

        if (EnergiserFallBack.getInstance() != null) EnergiserFallBack.getInstance().shutdown();

        energiserMap.values().forEach(Energiser::shutdown);
        energiserMap.clear();
        idCounter = new AtomicLong(0);

    }

    public static Energiser byId(long id) {

        Pair<Class, Long> key = energiserMap.keySet().stream().filter((pair) -> pair.getValue() == id).
                findFirst().
                orElse(null);

        return energiserMap.get(key);

    }

    public static Energiser byInitializer(Class clazz) {

        Pair<Class, Long> key = energiserMap.keySet().stream().filter((pair) -> pair.getKey() == clazz).
                findFirst().
                orElse(null);

        return energiserMap.get(key);

    }

    public static void alowedToInit(boolean bool) {
        allowedToInit = bool;
    }

    @Override
    public void schedule(ETask eTask) {

        if (scheduler == null) {

            EnergiserFallBack.getInstance().schedule(eTask);
            return;

        }


        if (scheduler.isShutdown()) return;

        scheduler.schedule(eTask);

    }

    @Override
    public void async(Runnable runnable, boolean pool) {

        ETask task = new ETask((t) -> runnable.run());
        if (scheduler == null) {

            EnergiserFallBack.getInstance().schedule(task);
            return;

        }

        if (!pool)
            scheduler.schedule(task);
        else {

            try {
                asyncPool.submit(() -> task.taskConsumer().accept(task));
            } catch (Exception ex) {
                getHandler().getErrorManager().error(ex.getMessage(), ex, 1);
            }

        }

    }

    @Override
    public ETask scheduleDelayed(Runnable runnable, long delay, TimeUnit unit) {

        if (scheduler == null) {
            Helper.print(unit.toSeconds(delay));
            ETask task = new ETask((t) -> runnable.run()).delay(unit.toMillis(delay));
            EnergiserFallBack.getInstance().schedule(task);
            return task;
        }
        if (scheduler.isShutdown()) return null;

        return scheduler.scheduleDelayed(runnable, delay, unit);

    }

    @Override
    public ETask scheduleRepeated(Runnable runnable, long timer, TimeUnit unit) {

        if (scheduler == null) {
            ETask task = new ETask((t) -> runnable.run()).delay(unit.toMillis(timer)).repeat(true);
            EnergiserFallBack.getInstance().schedule(task);
            return task;
        }
        if (scheduler.isShutdown()) return null;
        return scheduler.scheduleRepeated(runnable, timer, unit);

    }

    public void sync(Runnable runnable) {
        Bukkit.getScheduler().runTask(OOPCore.instance(), runnable);
    }

    public void shutdown() {

        if (asyncPool == null || scheduler == null) {
            energiserMap.remove(this);
            return;
        }

        try {
            asyncPool.shutdown();
            asyncPool.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            getHandler().getErrorManager().error(e.getMessage(), e, 3);
        } finally {
            asyncPool.shutdownNow();
        }

        scheduler.shutdown();
        energiserMap.remove(this);

    }

    public Handler getHandler() {
        return handler;
    }

    public Class<?> initializer() {
        return initializer;
    }

    public long id() {
        return id;
    }

}
