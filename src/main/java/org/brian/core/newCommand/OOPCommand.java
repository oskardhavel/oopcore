package org.brian.core.newCommand;

import lombok.Getter;
import org.brian.core.newCommand.arg.CmdArgument;
import org.brian.core.newCommand.req.SenderRequirement;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

@Getter
public class OOPCommand {

    //The name of the command
    private String label;

    //The description of the command
    private String description = "none";

    //If command will be shown when list of commands are shown / on tab complete
    private boolean secret = false;

    //Sender requirements
    private Set<SenderRequirement> requirementSet = new HashSet<>();

    //Aliases of the command
    private Set<String> aliases = new HashSet<>();

    //Permission of the command / No permission message
    private String permission = "none", noPermissionMessage = "&c&l(!)&7 You don't have permission to use this command!";

    //Sub commands
    private Map<String, OOPCommand> subCommands = new HashMap<>();

    //Args
    private Map<String, CmdArgument> argumentMap = new HashMap<>();

    //Listener
    private Consumer<WrappedCommand> listener;

    //Tab completion
    private Map<Integer, BiConsumer<List<String>, String[]>> tabComplete = new HashMap<>();

    //Parent
    private OOPCommand parent;

    public OOPCommand label(String label) {
        this.label = label;
        return this;
    }

    public OOPCommand description(String description) {
        this.description = description;
        return this;
    }

    public OOPCommand secret(boolean secret) {
        this.secret = secret;
        return this;
    }

    public OOPCommand requirement(SenderRequirement requirement) {
        this.requirementSet.add(requirement);
        return this;
    }

    public OOPCommand alias(String alias) {
        this.aliases.add(alias);
        return this;
    }

    public OOPCommand permission(String permission) {
        this.permission = permission;
        return this;
    }

    public OOPCommand noPermissionMessage(String noPermissionMessage) {
        this.noPermissionMessage = noPermissionMessage;
        return this;
    }

    public OOPCommand subCommand(OOPCommand command) {
        this.subCommands.put(command.getLabel(), command.parent(this));
        return this;
    }

    public OOPCommand argument(CmdArgument argument) {
        argumentMap.put(argument.getIdentifier(), argument);
        return this;
    }

    public OOPCommand listen(Consumer<WrappedCommand> listener) {
        this.listener = listener;
        return this;
    }

    public OOPCommand tabComplete(Integer place, BiConsumer<List<String>, String[]> completion){
        this.tabComplete.put(place, completion);
        return this;
    }

    public OOPCommand parent(){
        return parent;
    }

    public OOPCommand parent(OOPCommand parent){
        this.parent = parent;
        return this;
    }

}
