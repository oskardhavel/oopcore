package org.brian.core.newCommand.req;

import lombok.Getter;
import org.apache.commons.math3.util.Pair;
import org.bukkit.command.CommandSender;

@Getter
public class SenderRequirement {

    private ReqMapper reqMapper;

    public abstract class ReqMapper {

        public abstract Pair<Boolean, String> accepts(CommandSender sender);

    }

}
