package org.brian.core.newCommand;

import org.apache.commons.math3.util.Pair;
import org.brian.core.newCommand.arg.CmdArgument;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.stream.Collectors;

public class Test {

    void test() {

        OOPCommand oopCommand = new OOPCommand().
                label("testCommand").
                alias("tc").
                alias("tsc").
                description("Test Command!").
                tabComplete(1, (completion, args) ->
                        completion.addAll(Bukkit.getOnlinePlayers().
                                stream().
                                map(Player::getName).
                                collect(Collectors.toList()))).
                argument(
                        new CmdArgument().
                                description("An online player").
                                identity("player").
                                required(false).
                                mapper(new CmdArgument.ArgumentMapper(Player.class) {
                                    @Override
                                    public Pair<Player, String> product(String input) {

                                        Player player = Bukkit.getPlayer(input);
                                        if (player == null)
                                            return new Pair<>(null, "Failed to find player by name: " + input);
                                        else
                                            return new Pair<>(player, null);

                                    }
                                })
                ).
                listen(command -> {


                });

    }

}
