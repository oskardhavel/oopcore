package org.brian.core.newCommand;

import org.apache.commons.math3.util.Pair;
import org.brian.core.message.LineContent;
import org.brian.core.message.MessageLine;
import org.brian.core.message.OOPMessage;
import org.brian.core.newCommand.arg.CmdArgument;
import org.brian.core.newCommand.req.SenderRequirement;
import org.brian.core.plugin.OOPPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.SimplePluginManager;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class CommandController {

    private static CommandMap commandMap;
    private static boolean init = false;

    private OOPPlugin plugin;
    private ProperUsage properUsage;

    public CommandController(OOPPlugin plugin) {

        this.plugin = plugin;
        this.properUsage = ProperUsage.newInstance();
        if (!init) {
            try {

                Field cMap = SimplePluginManager.class.getDeclaredField("commandMap");
                cMap.setAccessible(true);
                commandMap = (CommandMap) cMap.get(Bukkit.getPluginManager());
                init = true;

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    public void register(OOPCommand command) {

        commandMap.register(plugin.getName(), new Command(command.getLabel(), command.getDescription(), "none", new ArrayList<>(command.getAliases())) {
            @Override
            public boolean execute(CommandSender sender, String cmdName, String[] args) {

                if (args.length == 0)
                    handleCommand(args, sender, command);
                else {

                    String secondArg = args[0];
                    if (command.getSubCommands().containsKey(secondArg))
                        handleCommand(cutArray(args, 1), sender, command.getSubCommands().get(args[0]));
                    else
                        handleCommand(args, sender, command);

                }

                return true;
            }
        });

    }

    private String colorize(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }

    private void handleCommand(String[] args, CommandSender sender, OOPCommand command) {

        //Check for permission
        if (!command.getPermission().equalsIgnoreCase("none") && !sender.hasPermission(command.getPermission())) {

            sender.sendMessage(colorize(command.getNoPermissionMessage()));
            return;

        }

        //Check for sender requirements
        for (SenderRequirement req : command.getRequirementSet()) {

            Pair<Boolean, String> mapped = req.getReqMapper().accepts(sender);
            if (!mapped.getKey()) {

                sender.sendMessage(colorize(mapped.getValue()));
                return;

            }

        }
        if (args.length >= 1 && command.getSubCommands().containsKey(args[0])) {
            handleCommand(cutArray(args, 1), sender, command.getSubCommands().get(args[0]));
            return;
        } else if(command.getListener() == null) {
            handleProperUsage(command, sender);
            return;
        }

        Map<String, Object> arguments = new HashMap<>();

        if (command.getArgumentMap().isEmpty())
            execCommand(new WrappedCommand(sender, arguments), command);
        else {

            String[] argsCopy = args.clone();
            for (CmdArgument arg : command.getArgumentMap().values()) {

                if (argsCopy.length == 0) {

                    if (!arg.isRequired()) continue;
                    handleProperUsage(command, sender);
                    return;

                }

                String stringValue = argsCopy[0];
                if (arg.getMapper() != null) {

                    Pair<Object, String> value = arg.getMapper().product(stringValue);
                    if (value.getKey() == null) {

                        sender.sendMessage(colorize(value.getValue()));
                        handleProperUsage(command, sender);
                        return;

                    } else {

                        arguments.put(arg.getIdentifier(), value.getKey());
                        argsCopy = cutArray(argsCopy, 1);

                    }
                }

            }

            execCommand(new WrappedCommand(sender, arguments), command);

        }


    }

    private void handleProperUsage(OOPCommand command, CommandSender sender) {

        //Check if simple message is required
        if (command.getSubCommands().isEmpty()) {
            OOPMessage message = new OOPMessage();
            MessageLine line = new MessageLine();
            line.append(properUsage.getMainColor() + "Usage: /" + (command.getParent() != null ? command.getParent().getLabel() + " " : ""));

            LineContent labelContent = new LineContent(properUsage.getMainColor() + command.getLabel());
            if (!command.getDescription().equalsIgnoreCase("none"))
                labelContent.hoverText(properUsage.getMainColor() + command.getDescription());
            line.append(labelContent);

            buildArgs(command.getArgumentMap().values(), line);
            message.appendLine(line);
            if(!command.getPermission().equalsIgnoreCase("none")){
                message.appendLine(properUsage.getMainColor() + "&l* " + properUsage.getMainColor() + "Permission: &f" + command.getPermission());
            }
            if(!command.getDescription().equalsIgnoreCase("none")) {
                message.appendLine(properUsage.getMainColor() + "&l* " + properUsage.getMainColor() + "Description: &f" + command.getDescription());
            }

            if (sender instanceof Player)
                message.send(((Player) sender));

        } else {

            boolean hasRequired = command.getSubCommands().values().stream().
                    map(cmd -> (int) cmd.getArgumentMap().values().stream().filter(CmdArgument::isRequired).count()).
                    anyMatch(c -> c > 0);
            boolean hasOptional = command.getSubCommands().values().stream().
                    map(cmd -> (int) cmd.getArgumentMap().values().stream().filter(a -> !a.isRequired()).count()).
                    anyMatch(c -> c > 0);

            //Advanced
            OOPMessage message = new OOPMessage();
            message.appendLine(properUsage.getSecondColor() + "&l---==== " + properUsage.getMainColor() + "Help");

            //Format Builder
            if (hasOptional && hasRequired)
                message.appendLine(properUsage.getSecondColor() + "   <> - Required, [] - Optional");

            else if (hasRequired)
                message.appendLine(properUsage.getSecondColor() + "   <> - Required");

            else if (hasOptional)
                message.appendLine(properUsage.getSecondColor() + "   [] - Optional");

            message.appendLine(" ");
            for (OOPCommand subCommand : command.getSubCommands().values()) {

                //Check if the command is hidden
                if(subCommand.isSecret())
                    continue;

                //Check for permission (if sender has the permission to use this command)
                if(subCommand.getPermission().equalsIgnoreCase("none") && !sender.hasPermission(subCommand.getPermission()))
                    continue;

                MessageLine line = new MessageLine();
                line.append(properUsage.getMainColor() + "/" + command.getLabel() + " ");

                LineContent labelContent = new LineContent(properUsage.getMainColor() + subCommand.getLabel());
                if (!subCommand.getDescription().equalsIgnoreCase("none"))
                    labelContent.hoverText(properUsage.getMainColor() + subCommand.getDescription());
                line.append(labelContent);

                buildArgs(subCommand.getArgumentMap().values(), line);
                if(!subCommand.getDescription().equalsIgnoreCase("")) {
                    line.append(properUsage.getMainColor() + " - " + subCommand.getDescription());
                }
                message.appendLine(line);

            }
            message.appendLine(" ");
            if(sender instanceof Player)
                message.send(((Player) sender));

        }

    }

    private void execCommand(WrappedCommand command, OOPCommand oopCommand) {
        oopCommand.getListener().accept(command);
    }

    private String[] cutArray(String[] array, int amount) {
        if (array.length <= amount)
            return new String[0];
        else
            return Arrays.copyOfRange(array, amount, array.length);
    }

    public void properUsage(ProperUsage properUsage) {
        this.properUsage = properUsage;
    }

    private void buildArgs(Collection<CmdArgument> args, MessageLine line) {

        //Format = <required> [optional]
        AtomicBoolean first = new AtomicBoolean(true);
        if (args.stream().anyMatch(CmdArgument::isRequired)) {

            line.append(properUsage.getMainColor() + " <");
            args.stream().filter(CmdArgument::isRequired).forEach(arg -> {

                if (!first.get())
                    line.append(", ");
                else
                    first.set(false);
                LineContent content = new LineContent("&f" + arg.getIdentifier()).hoverText(properUsage.getMainColor() + arg.getDescription());
                line.append(content);

            });
            line.append(properUsage.getMainColor() + ">");

        }
        first.set(true);

        if (args.stream().anyMatch(a -> !a.isRequired())) {

            line.append(properUsage.getSecondColor() + " [");
            args.stream().filter(a -> !a.isRequired()).forEach(arg -> {

                if (!first.get()) {
                    line.append(", ");
                } else
                    first.set(false);
                LineContent content = new LineContent("&f" + arg.getIdentifier()).hoverText(properUsage.getSecondColor() + arg.getDescription());
                line.append(content);

            });
            line.append(properUsage.getSecondColor() + "]" + properUsage.getMarkupColor());

        }

    }

}
