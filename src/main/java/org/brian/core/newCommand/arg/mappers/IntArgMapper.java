package org.brian.core.newCommand.arg.mappers;

import org.apache.commons.math3.util.Pair;
import org.brian.core.newCommand.arg.CmdArgument;

public class IntArgMapper extends CmdArgument.ArgumentMapper<Integer> {

    public IntArgMapper(){
        super(Integer.class);
    }

    @Override
    public Pair<Integer, String> product(String input) {

        boolean isInteger = false;
        try{
            Integer.parseInt(input);
            isInteger = true;
        } catch (Exception ex){}

        if(!isInteger)
            return new Pair<>(null, "Failed to parse " + input + " as an Integer!");
        else
            return new Pair<>(Integer.parseInt(input), "none");
    }
}
