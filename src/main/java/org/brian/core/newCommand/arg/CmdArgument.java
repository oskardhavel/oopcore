package org.brian.core.newCommand.arg;

import lombok.Getter;
import org.apache.commons.math3.util.Pair;

@Getter
public class CmdArgument {

    //Argument parser / Will return as String if not defined
    private ArgumentMapper mapper;

    //Will be used on when list of the commands are sent / On proper command usage / When getting the arguments
    private String identifier = "";

    //Will be used as hover text in list of the commands are sent / On proper command usage
    private String description = "";

    //If it's required command won't run if not found, else it will run.
    private boolean required = false;

    public static abstract class ArgumentMapper<R> {

        private Class<R> type;
        public ArgumentMapper(Class<R> type){
            this.type = type;
        }

        //Returns Parsed value and if failed to parse value error message.
        public abstract Pair<R, String> product(String input);

    }

    public CmdArgument mapper(ArgumentMapper mapper){
        this.mapper = mapper;
        return this;
    }

    public CmdArgument identity(String identifier){
        this.identifier = identifier;
        return this;
    }

    public CmdArgument description(String description){
        this.description = description;
        return this;
    }

    public CmdArgument required(boolean required){
        this.required = required;
        return this;
    }

}
