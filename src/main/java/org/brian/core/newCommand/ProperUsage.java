package org.brian.core.newCommand;

import lombok.Getter;

@Getter
public class ProperUsage {

    private String mainColor = "&e";
    private String secondColor = "&3";
    private String markupColor = "&7";

    private ProperUsage() {
    }

    public static ProperUsage newInstance() {
        return new ProperUsage();
    }

    public ProperUsage mainColor(String mainColor) {
        this.mainColor = mainColor;
        return this;
    }

    public ProperUsage secondColor(String secondColor) {
        this.secondColor = secondColor;
        return this;
    }

    public ProperUsage markupColor(String markupColor){
        this.markupColor = markupColor;
        return this;
    }

}
