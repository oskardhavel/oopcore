package org.brian.core.newCommand;

import org.bukkit.command.CommandSender;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class WrappedCommand {

    private CommandSender sender;

    public WrappedCommand(CommandSender sender, Map<String, Object> arguments) {
        this.sender = sender;
        this.arguments = arguments;
    }

    private Map<String, Object> arguments;

    public CommandSender sender() {
        return sender;
    }

    public Optional<Object> arg(String arg) {
        return Optional.ofNullable(arguments.get(arg));
    }

    public <T> T argAsRequired(String arg) {
        return (T) arguments.get(arg);
    }
}
