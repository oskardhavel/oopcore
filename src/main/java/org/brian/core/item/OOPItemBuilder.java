package org.brian.core.item;

import org.brian.core.OOPCore;
import org.brian.core.material.OOPMaterial;
import org.brian.core.nbt.NBTItem;
import org.brian.core.utils.Assert;
import org.brian.core.yaml.ConfigurationSection;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import static org.brian.core.utils.Helper.color;

public class OOPItemBuilder {

    private ItemStack item;
    private List<String> lore = new ArrayList<>();
    private List<ItemFlag> flags = new ArrayList<>();
    private List<ItemFlag> flagsToRemove = new ArrayList<>();
    private HashMap<String, Object> nbt_toAdd = new HashMap<>();
    private List<IEnchant> enchants_toAdd = new ArrayList<>();
    private List<Enchantment> enchants_toRemove = new ArrayList<>();
    private List<String> nbt_toRemove = new ArrayList<>();
    private String name;
    private boolean glow = false;

    public OOPItemBuilder(Material mat) {
        this(mat, 1);
    }

    public OOPItemBuilder(Material mat, int amount) {
        this(new ItemStack(mat, amount));
    }

    public OOPItemBuilder(ItemStack item) {
        this.item = item;

        if (item.getItemMeta() != null) {
            if (item.getItemMeta().hasLore()) lore(item.getItemMeta().getLore(), true);
            if (item.getItemMeta().hasDisplayName()) displayName(item.getItemMeta().getDisplayName());
        }

    }

    public OOPItemBuilder(OOPMaterial material) {
        this.item = material.parseItem();
    }

    public static OOPItemBuilder fromMap(Map<String, Object> data) {

        if (!data.containsKey("material")) {
            throw new IllegalStateException("Failed to initialize ItemStack there's required key missing material!");
        }

        OOPItemBuilder builder = null;
        try {
            builder = new OOPItemBuilder(OOPMaterial.fromString(data.get("material").toString().toUpperCase()).parseItem());
        } catch (NullPointerException ex) {
            OOPCore.instance().logger().error(ex);
        }

        if (data.containsKey("display name")) {
            builder.displayName((String) data.get("display name"));
        }

        if (data.containsKey("lore")) {
            builder.lore((List<String>) data.get("lore"), true);
        }

        if (data.containsKey("glow")) {

            builder.glow(Boolean.valueOf(data.get("glow").toString()));

        }

        return builder;

    }

    public static OOPItemBuilder fromSection(ConfigurationSection section) {

        Assert.assertTrue("Failed to initialize ItemStack, missing (material)!", section.isPresentValue("material"));
        String stringMaterial = section.valueAsRequired("material");
        OOPMaterial material = OOPMaterial.fromString(stringMaterial.toUpperCase());
        Assert.assertTrue("Failed to initialize OOPItemBuilder, material is invalid! (" + stringMaterial + ", section: " + section.key() + ")", material != null);

        OOPItemBuilder itemBuilder = new OOPItemBuilder(material);
        if (section.isPresentValue("display name"))
            itemBuilder.displayName(section.valueAsRequired("display name"));

        if (section.isPresentValue("lore"))
            itemBuilder.lore(section.valueAsRequired("lore"), true);

        if (section.isPresentValue("glow"))
            itemBuilder.glow(section.valueAsRequired("glow"));

        return itemBuilder;
    }

    public OOPItemBuilder addNbt(String key, Object ob2) {
        nbt_toAdd.put(key, ob2);
        return this;
    }

    public OOPItemBuilder appendLore(String string) {
        lore.add(color(string));
        return this;
    }

    public OOPItemBuilder removeLoreLine(int index) {
        lore.remove(index);
        return this;
    }

    public OOPItemBuilder displayName(String s) {
        name = s;
        return this;
    }

    public OOPItemBuilder loreLine(Integer index, String string) {
        lore.set(index, color(string));
        return this;
    }

    public ItemStack buildItem() {

        if (glow) item.addUnsafeEnchantment(Enchantment.LUCK, 1);
        ItemMeta meta = item.getItemMeta();

        if (meta != null) {
            meta.setLore(lore);
            if (name != null) {
                meta.setDisplayName(color(name));
            }

            for (ItemFlag flag : flagsToRemove) {

                meta.removeItemFlags(flag);

            }
            for (ItemFlag flag : flags) {

                meta.addItemFlags(flag);

            }

            for (Enchantment enchant : enchants_toRemove) {

                meta.removeEnchant(enchant);

            }

            for (IEnchant enchantment : enchants_toAdd) {

                meta.addEnchant(enchantment.getEnchantment(), enchantment.getSize(), true);

            }

            if (glow) meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);

            item.setItemMeta(meta);
        }

        NBTItem nbt = new NBTItem(item);
        for (String key : nbt_toAdd.keySet()) {

            if (nbt_toAdd.get(key) instanceof String) nbt.setString(key, nbt_toAdd.get(key).toString());
            else nbt.setObject(key, nbt_toAdd.get(key));

        }
        for (String s : nbt_toRemove) {

            nbt.removeKey(s);

        }
        return nbt.getItem();
    }

    public OOPItemBuilder removeFromNbt(String key) {
        nbt_toRemove.add(key);
        return this;
    }

    public OOPItemBuilder clearLore() {
        lore.clear();
        return this;
    }

    public OOPItemBuilder lore(List<String> l) {
        lore = l;
        return this;
    }

    public OOPItemBuilder replaceInLore(String key, String to) {
        List<String> copy = new ArrayList<>(lore);
        lore.clear();
        for (String s : copy) {
            s = s.replaceAll(key, to);
            lore.add(s);
        }
        return this;
    }

    public OOPItemBuilder random(String key) {
        nbt_toAdd.put(key + "_randomness_" + ThreadLocalRandom.current().nextInt(999999), key + "_randomness_" + ThreadLocalRandom.current().nextInt(999999));
        return this;
    }

    public OOPItemBuilder random() {
        nbt_toAdd.put("_randomness_" + ThreadLocalRandom.current().nextInt(999999), "_randomness_" + ThreadLocalRandom.current().nextInt(999999));
        return this;
    }

    public OOPItemBuilder addItemFlag(ItemFlag flag) {

        flags.add(flag);
        return this;

    }

    public OOPItemBuilder removeItemFlag(ItemFlag flag) {

        flagsToRemove.remove(flag);
        return this;

    }

    public OOPItemBuilder addEnchant(Enchantment enchant, int level) {

        enchants_toAdd.add(new IEnchant(enchant, level));
        return this;

    }

    public OOPItemBuilder removeEnchant(Enchantment enchant) {

        enchants_toRemove.add(enchant);
        return this;

    }

    public OOPItemBuilder lore(List<String> lore, boolean coloured) {

        if (coloured) {

            List<String> s = new ArrayList<>();
            lore.forEach(s1 -> s.add(color(s1)));
            this.lore = s;

        }
        return this;

    }

    public OOPItemBuilder replaceLore(Map<String, Object> replace) {

        List<String> newLore = new ArrayList<>();

        for (String l : this.lore) {

            for (String r : replace.keySet()) {
                l = l.replaceAll(r, replace.get(r).toString());
            }
            newLore.add(l);

        }
        this.lore = newLore;
        return this;
    }

    public OOPItemBuilder durability(byte data) {
        item.setDurability(data);
        return this;
    }

    public void glow(boolean glow) {
        this.glow = glow;
    }

    public boolean glow() {
        return this.glow;
    }

    public OOPItemBuilder durability(int data) {
        return durability((byte) data);
    }

    public String displayName() {
        return this.name;
    }

    public void material(Material type) {
        this.item.setType(type);
    }

}
