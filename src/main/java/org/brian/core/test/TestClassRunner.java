package org.brian.core.test;

import lombok.Getter;
import org.brian.core.OOPCore;
import org.brian.core.yaml.OOPConfiguration;
import org.brian.core.yaml.annotations.ConfigValue;
import org.brian.core.yaml.annotations.Configurable;
import org.brian.core.yaml.annotations.SectionValue;

import java.util.ArrayList;
import java.util.List;

public class TestClassRunner {

    public TestClassRunner(OOPCore oopCore) {

    }

    @Getter
    public class FieldsClass implements Configurable {

        @ConfigValue(path = "testValue")
        private int testValue;
        @SectionValue(path = "testSection")
        private TestClass singleSection;
        @SectionValue(path = "multipleSections")
        private List<TestClass> multipleSections = new ArrayList<>();

        public FieldsClass(OOPConfiguration configuration) {
            try {
                initConfig(configuration);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

}
