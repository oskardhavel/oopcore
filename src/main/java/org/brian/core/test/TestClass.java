package org.brian.core.test;

import lombok.Getter;

@Getter
public class TestClass {

    private int age;
    private String name;

    public TestClass(int age, String name) {
        this.age = age;
        this.name = name;
    }
}
