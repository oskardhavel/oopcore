package org.brian.core.events;

import org.brian.core.OOPCore;
import org.brian.core.events.async.AsyncEvent;
import org.brian.core.events.async.EventData;
import org.brian.core.plugin.OOPPlugin;
import org.brian.core.utils.Helper;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.EventExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public interface AsyncEvents extends Listener, EventExecutor {

    List<AsyncEvents> registeredEvents = new ArrayList<>();

    static <T extends Event> AsyncEvents listen(
            Class<T> type,
            Consumer<T> listener,
            OOPPlugin plugin
    ) {
        return listen(type, EventPriority.NORMAL, listener, plugin);
    }

    static <T extends Event> AsyncEvents listen(
            Class<T> type,
            EventPriority priority,
            Consumer<T> listener,
            OOPPlugin plugin
    ) {

        final AsyncEvents events = ($, event) -> {

            if (type.isInstance(event)) {
                async(() -> listener.accept(type.cast(event)));
            }

        };
        Bukkit.getPluginManager().registerEvent(type, events, priority, events, plugin);
        registeredEvents.add(events);
        return events;
    }

    static <T extends Event> AsyncEvents listen(
            Class<T> type,
            Consumer<T> listener,
            Consumer<T> preAsync,
            OOPPlugin plugin
    ) {

        final AsyncEvents events = ($, event) -> {
            if (type.isInstance(event)) {
                preAsync.accept(type.cast(event));
                async(() -> listener.accept(type.cast(event)));
            }
        };
        Bukkit.getPluginManager().registerEvent(type, events, EventPriority.NORMAL, events, plugin);
        registeredEvents.add(events);
        return events;
    }

    static <T extends Event> AsyncEvents listen(
            AsyncEvent<T> asyncEvent,
            OOPPlugin plugin
    ) {

        final AsyncEvents events = ($, event) -> {
            if (asyncEvent.getClassType().isInstance(event)) {

                Helper.print(Thread.currentThread().getName());
                EventData data = new EventData();
                if (EventData.getDataTypes().containsKey(asyncEvent.getClassType()))
                    EventData.getDataType(asyncEvent.getClassType()).accept(asyncEvent.getClassType().cast(event), data);

                if (asyncEvent.getPreAsync() != null)
                    asyncEvent.getPreAsync().accept(asyncEvent.getClassType().cast(event), data);

                async(() -> {
                    if (asyncEvent.getAsync() != null)
                        asyncEvent.getAsync().accept(asyncEvent.getClassType().cast(event), data);
                });

            }
        };
        if (asyncEvent.getEventPriority() != null)
            Bukkit.getPluginManager().registerEvent(asyncEvent.getClassType(), events, asyncEvent.getEventPriority(), events, plugin);
        else
            Bukkit.getPluginManager().registerEvent(asyncEvent.getClassType(), events, asyncEvent.getEventPriority(), events, plugin);
        registeredEvents.add(events);

        return events;
    }

    static void async(Runnable runnable) {
        OOPCore.instance().energiser().async(runnable, false);
    }

    static void unregister() {
        registeredEvents.forEach(HandlerList::unregisterAll);
    }

}
