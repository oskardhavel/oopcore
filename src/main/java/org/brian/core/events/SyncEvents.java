package org.brian.core.events;

import org.brian.core.plugin.OOPPlugin;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.EventExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public interface SyncEvents extends Listener, EventExecutor {

    List<SyncEvents> registeredEvents = new ArrayList<>();

    static <T extends Event> SyncEvents listen(
            Class<T> type,
            Consumer<T> listener,
            OOPPlugin plugin
    ) {

        final SyncEvents events = ($, event) -> {
            if (type.isInstance(event)) {
                listener.accept(type.cast(event));
            }
        };
        Bukkit.getPluginManager().registerEvent(type, events, EventPriority.NORMAL, events, plugin);
        registeredEvents.add(events);
        return events;
    }

    static void unregister() {
        registeredEvents.forEach(HandlerList::unregisterAll);
    }

}

