package org.brian.core;

import org.apache.commons.lang3.StringUtils;
import org.brian.core.controller.PlayerController;
import org.brian.core.events.SyncEvents;
import org.brian.core.player.OOPPLayer;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

class CoreListener {

    public CoreListener(OOPCore core) {

        SyncEvents.listen(PlayerCommandPreprocessEvent.class, event -> {

            if (event.getMessage().startsWith("/oopCore:action:consumer:")) {

                event.setCancelled(true);

                OOPPLayer oopPlayer = PlayerController.instance().get(event.getPlayer());
                long id = Long.parseLong(StringUtils.replace(event.getMessage(), "/oopCore:action:consumer:", ""));

                if (oopPlayer.containsConsumer(id)) oopPlayer.getConsumer(id).accept(event.getPlayer());

            }

        }, OOPCore.instance());

    }

}
