package org.brian.core;

import org.brian.core.item.OOPItemBuilder;
import org.brian.core.message.OOPMessage;
import org.brian.core.yaml.ConfigurationSection;
import org.brian.core.yaml.mapper.section.SectionMapper;
import org.bukkit.inventory.ItemStack;

class Premade {

    private static boolean inited = false;

    public static void init() {

        if (!inited) {
            inited = true;

            //OOPMessage builder
            new SectionMapper<OOPMessage>(OOPMessage.class) {
                @Override
                public OOPMessage map(ConfigurationSection section) {
                    return OOPMessage.fromSection(section);
                }
            }.register();

            //ItemStack builder
            new SectionMapper<ItemStack>(ItemStack.class) {
                @Override
                public ItemStack map(ConfigurationSection section) {
                    return OOPItemBuilder.fromSection(section).buildItem();
                }
            }.register();

        }

    }

}
