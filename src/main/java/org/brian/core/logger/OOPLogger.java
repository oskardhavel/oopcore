package org.brian.core.logger;

import com.google.common.collect.HashBiMap;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.Pair;
import org.brian.core.plugin.OOPPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.concurrent.atomic.AtomicLong;

public class OOPLogger {

    private static HashBiMap<Pair<Class, Long>, OOPLogger> loggerMap = HashBiMap.create();
    private static AtomicLong idCounter = new AtomicLong(0);
    private String error_prefix = "&c[OOPLogger ERROR]: &7";
    private String warn_prefix = "&4[OOPLogger WARN]: &7";
    private String normal_prefix = "&e[OOPLogger]: &7";
    private String debug_prefix = "&e[OOPLogger DEBUG]: &7";
    @Getter
    private Class<?> initializer;
    @Setter

    @Getter
    private long id;
    @Getter
    private String loggerName;

    private OOPLogger(String name, String error_prefix, String warn_prefix, String normal_prefix, String debug_prefix, Class initializer) {

        if (error_prefix != null) this.error_prefix = error_prefix;
        if (warn_prefix != null) this.warn_prefix = warn_prefix;
        if (normal_prefix != null) this.normal_prefix = normal_prefix;
        if (debug_prefix != null) this.debug_prefix = debug_prefix;
        this.initializer = initializer;
        if (name != null) this.loggerName = name;

        register(this);

    }

    @Builder
    private static OOPLogger of(String name, String error_prefix, String warn_prefix, String normal_prefix, String debug_prefix, Class initializer) {
        return new OOPLogger(name, error_prefix, warn_prefix, normal_prefix, debug_prefix, initializer);
    }

    public static OOPLogger byId(long id) {

        Pair<Class, Long> key = loggerMap.keySet().stream().filter((pair) -> pair.getValue() == id).
                findFirst().
                orElse(null);

        return loggerMap.get(key);

    }

    public static OOPLogger byInitializer(Class clazz) {

        Pair<Class, Long> key = loggerMap.keySet().stream().filter((pair) -> pair.getKey() == clazz).
                findFirst().
                orElse(null);

        return loggerMap.get(key);

    }

    private static void register(OOPLogger logger) {

        if (logger.getInitializer() == null)
            throw new IllegalStateException("Failed to initialize OOPLogger! Initializer is not defined!");

        long id = idCounter.incrementAndGet();
        logger.setId(id);

        loggerMap.put(new Pair<>(logger.getInitializer(), id), logger);

    }

    public OOPLogger name(String name) {
        this.loggerName = name;
        this.error_prefix = StringUtils.replace(error_prefix, "OOPLogger", loggerName);
        this.warn_prefix = StringUtils.replace(warn_prefix, "OOPLogger", loggerName);
        this.normal_prefix = StringUtils.replace(normal_prefix, "OOPLogger", loggerName);
        this.debug_prefix = StringUtils.replace(debug_prefix, "OOPLogger", loggerName);
        return this;
    }

    public OOPLogger name(OOPPlugin plugin) {
        return name(plugin.getName());
    }

    private void send(String message) {
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    public void printEmpty() {
        send("");
    }

    public void print(Object object) {
        send(normal_prefix + object.toString());
    }

    public void printError(Object object) {
        send(error_prefix + object.toString());
    }

    public void printWarning(Object object) {
        send(warn_prefix + object.toString());
    }

    public void printDebug(Object object) {
        send(debug_prefix + object.toString());
    }

    public void error(Exception exception) {
        printError("Exception was caught in " + getLoggerName() + ": " + exception.getMessage());
    }

}
