package org.brian.core;

import org.brian.core.controller.PlayerController;
import org.brian.core.energiser.Energiser;
import org.brian.core.energiser.EnergiserFallBack;
import org.brian.core.logger.OOPLogger;
import org.brian.core.mi.MenuListener;
import org.brian.core.plugin.OOPPlugin;
import org.brian.core.test.TestClassRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OOPCore extends OOPPlugin {

    private static OOPCore instance;
    private OOPLogger coreLogger;
    private CoreHandler coreHandler;
    private Energiser coreEnergiser;

    private List<OOPPlugin> hooked = new ArrayList<>();

    public static OOPCore instance() {
        return instance;
    }

    @Override
    public void enable() {

        instance = this;
        Premade.init();
        try {
            new EnergiserFallBack(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        this.coreLogger = OOPLogger.builder()
                .initializer(OOPCore.class).
                        build().
                        name(this);
        this.coreHandler = new CoreHandler();

        logger().print("Initializing &eCore v" + getDescription().getVersion() + "..");

        if (Runtime.getRuntime().availableProcessors() < 3) {
            logger().printWarning("Minimum 3 threads is required! Using fallback scheduler (Bukkit)");
            Energiser.alowedToInit(false);
        }

        this.coreEnergiser = new Energiser("Core", coreHandler, OOPCore.class);
        new PlayerController();
        new CoreListener(this);
        new MenuListener();

        logger().print("&cCore is made by OOP-778. Support Discord > https://discord.gg/Z9bjuVr");

        //Testing part
        if (getDataFolder().exists()) getDataFolder().mkdirs();

        new TestClassRunner(this);

    }

    @Override
    public void disable() {
        Energiser.shutdownAll();
        hooked.forEach(OOPPlugin::onDisable);
    }

    public OOPLogger logger() {
        return coreLogger;
    }

    public CoreHandler handler() {
        return coreHandler;
    }

    public Energiser energiser() {
        return coreEnergiser;
    }

    public void hook(OOPPlugin plugin) {

        //Ignore core itself
        if (plugin == this) return;

        this.hooked.add(plugin);
        logger().print("&e" + plugin.getName() + "&7 has hooked to the core! &e(v" + plugin.getDescription().getVersion() + ")");
    }

    public void unhook(OOPPlugin plugin) {

        //Ignore core itself
        if (plugin == this) return;

        this.hooked.remove(plugin);
        logger().print(plugin.getName() + " has unhooked from the core!");
    }

}
