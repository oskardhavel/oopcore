package org.brian.core;

import org.brian.core.file.OOPFile;
import org.brian.core.message.LineContent;
import org.brian.core.message.MessageLine;
import org.brian.core.message.OOPMessage;
import org.brian.core.message.additions.action.CommandAddition;
import org.brian.core.yaml.ConfigurationSection;
import org.brian.core.yaml.OOPConfiguration;
import org.brian.core.yaml.annotations.Configurable;
import org.brian.core.yaml.annotations.SectionValue;

import java.io.File;

public class Runner {

    public static void main(String[] args) {

        Premade.init();
        Runner.class.getClassLoader().getResource("config.yml").getPath();
        String path = Runner.class.getClassLoader().getResource("config.yml").getPath().replace("/config.yml", "");

        OOPFile oopFile = new OOPFile(new File(path), "test.yml");
        oopFile.createIfNotExists();

        OOPConfiguration configuration = new OOPConfiguration(oopFile.file());

        //Build Test Message
        OOPMessage testMessage = new OOPMessage().center(true).
                appendLine("").
                appendLine("&e&lLink New Container").
                appendLine(new MessageLine().append(new LineContent("&cClick to quit linking mode!").hoverText("&cClick to quit linking mode!").addAddition(new CommandAddition().command("/eh quitLinking"))).append(" &cOIIIIIF")).
                appendLine("");


        configuration.appendHeader("Plugin made by UnknownDeveloper");
        configuration.appendHeader("Support is provided at -> discord url");

        testMessage.saveToConfig(configuration, "test");
        configuration.set("test timer", 10000);
        configuration.set("test.center", true);

        configuration.save();

        ConfigurationSection messageSection = configuration.section("test").getFirst();
        OOPMessage message = OOPMessage.fromSection(messageSection);

        message.getLineList().forEach(line -> {
            line.contentList().forEach(content -> {
                System.out.println(content.getText());
                System.out.println(content.getAdditionList().size());
            });
        });


    }

    static class Test implements Configurable {

        //Maps single section to defined object
        @SectionValue(path = "test")
        private OOPMessage message;

        public Test(OOPConfiguration configuration) {
            try {
                initConfig(configuration);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            System.out.println(message.getLineList().size());
        }


    }

}
