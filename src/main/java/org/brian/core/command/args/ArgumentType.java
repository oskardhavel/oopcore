package org.brian.core.command.args;

import java.util.function.Predicate;

public class ArgumentType {

    private Predicate<String> check;
    private String checkFailedMessage;

    public ArgumentType() {
    }

    public Predicate<String> getCheck() {
        return check;
    }

    public ArgumentType setCheck(Predicate<String> check) {
        this.check = check;
        return this;
    }

    public String getCheckFailedMessage() {
        return checkFailedMessage;
    }

    public ArgumentType setCheckFailedMessage(String checkFailedMessage) {
        this.checkFailedMessage = checkFailedMessage;
        return this;
    }
}
