package org.brian.core;

import org.brian.core.utils.Helper;

import java.util.logging.ErrorManager;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class CoreHandler extends Handler {

    @Override
    public void publish(LogRecord record) {

    }

    @Override
    public void flush() {

    }

    @Override
    public void close() throws SecurityException {

    }

    public class CoreErrorManager extends ErrorManager {

        @Override
        public synchronized void error(String msg, Exception ex, int code) {
            Helper.print("");
            Helper.print("&cException was thrown in ExclusiveHoppers: " + ex.getMessage());

            for (StackTraceElement ste : ex.getStackTrace()) {
                Helper.print("&c - " + ste.toString());
            }

            Helper.print("");
        }
    }

}
