package org.brian.core.utils;

public class Assert {

    public static void assertTrue(String message, boolean condition) {
        if (!condition) {
            fail(message);
        }

    }

    public static void assertTrue(boolean condition) {
        assertTrue(null, condition);
    }

    public static void assertFalse(String message, boolean condition) {
        assertTrue(message, !condition);
    }

    public static void assertFalse(boolean condition) {
        assertFalse(null, condition);
    }

    public static void fail(String message) {
        if (message == null) {
            throw new AssertionError();
        } else {
            throw new AssertionError(message);
        }
    }

    public static void fail() {
        fail(null);
    }

}
