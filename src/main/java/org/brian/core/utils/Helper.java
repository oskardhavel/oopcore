package org.brian.core.utils;

import org.brian.core.OOPCore;
import org.brian.core.energiser.Energiser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Helper {

    public static <T> List<T> toObjectsList(T... elements) {
        return new ArrayList<>(Arrays.asList(elements));
    }

    public static void print(Object text) {
        Bukkit.getConsoleSender().sendMessage(color(text.toString()));
    }

    public static String color(String text) {
        return ChatColor.translateAlternateColorCodes('&', text);
    }

    public static boolean isAsyncThread() {
        return !Thread.currentThread().getName().equalsIgnoreCase("Server thread");
    }

    public static void async(Runnable runnable, boolean newThread) {

        Energiser energiser = Energiser.byInitializer(OOPCore.class);
        if (energiser == null) throw new IllegalStateException("Failed To Find Core Energiser!");
        energiser.async(runnable, newThread);

    }

    public static void sync(Runnable runnable) {

        if (!isAsyncThread()) {
            runnable.run();
            return;
        }

        Energiser energiser = Energiser.byInitializer(OOPCore.class);
        if (energiser == null) throw new IllegalStateException("Failed To Find Core Energiser!");
        energiser.sync(runnable);

    }

    public static void async(Runnable runnable) {
        async(runnable, false);
    }

    public static <T> List<List<T>> splitList(final List<T> ls, final int iParts) {
        final List<List<T>> lsParts = new ArrayList<>();
        final int iChunkSize = ls.size() / iParts;
        int iLeftOver = ls.size() % iParts;
        int iTake = iChunkSize;

        for (int i = 0, iT = ls.size(); i < iT; i += iTake) {
            if (iLeftOver > 0) {
                iLeftOver--;

                iTake = iChunkSize + 1;
            } else {
                iTake = iChunkSize;
            }

            lsParts.add(new ArrayList<>(ls.subList(i, Math.min(iT, i + iTake))));
        }

        return lsParts;
    }

    public static boolean collectionContains(Object object, Collection collection) {
        return collection.contains(object);
    }

    public static <T> boolean arrayContains(T object, T[] array) {
        return Arrays.stream(array).
                anyMatch(target -> target.equals(object));
    }

}
