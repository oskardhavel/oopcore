package org.brian.core.utils;

import java.util.HashMap;
import java.util.Map;

public abstract class Storagable {

    private Map<String, Object> data = new HashMap<>();

    public boolean containsData(String key) {
        return data.containsKey(key);
    }

    public void putData(String key, Object object) {

        data.remove(key);
        data.put(key, object);

    }

    public <T> T getData(String key) {

        if (!containsData(key)) return null;
        return (T) data.get(key);

    }

    public void removeData(String key) {
        data.remove(key);
    }

    public Map<String, Object> getData() {
        return data;
    }
}
