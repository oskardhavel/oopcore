package org.brian.core.mi.designer;

public enum RowType {

    LAST(),
    FIRST,
    NUMBER

}
