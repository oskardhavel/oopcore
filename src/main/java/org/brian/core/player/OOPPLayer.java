package org.brian.core.player;

import org.brian.core.utils.Storagable;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

public class OOPPLayer extends Storagable {

    private UUID uuid;
    private Map<Long, Consumer<Player>> consumerMap = new HashMap<>();

    public OOPPLayer(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID uuid() {
        return uuid;
    }

    public void registerConsumer(long id, Consumer<Player> consumer) {
        this.consumerMap.put(id, consumer);
    }

    public void unregisterConsumer(long id) {
        this.consumerMap.remove(id);
    }

    public boolean containsConsumer(long id) {
        return this.consumerMap.containsKey(id);
    }

    public Consumer<Player> getConsumer(long id) {
        return this.consumerMap.get(id);
    }
}
