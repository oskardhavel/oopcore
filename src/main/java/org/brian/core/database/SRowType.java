package org.brian.core.database;

public enum SRowType {

    INTEGER("INT"),
    FLOAT("LONG"),
    TEXT("TEXT"),
    VARCHAR("VARCHAR"),
    DOUBLE("DOUBLE"),
    BOOLEAN("BOOLEAN");

    private String sql;

    SRowType(String sql) {
        this.sql = sql;
    }

    public static String fromObject(Object obj) {

        if (obj instanceof Integer) return obj.toString() + " " + INTEGER.sql;
        if (obj instanceof Float) return obj.toString() + " " + FLOAT.sql;
        if (obj instanceof Double) return obj.toString() + " " + DOUBLE.sql;
        if (obj instanceof Boolean) return obj.toString() + " " + BOOLEAN.sql;
        if (obj instanceof String) return obj.toString() + " " + VARCHAR.sql;
        return obj.toString() + " " + TEXT.sql;

    }

    public String getSql() {
        return sql;
    }
}
