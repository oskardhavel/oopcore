package org.brian.core.database;

import org.brian.core.utils.Helper;

import java.sql.*;
import java.util.HashMap;

public abstract class OOPDatabase {

    private SDatabaseType type;
    private String url;

    public OOPDatabase(String url, SDatabaseType type) {

        this.type = type;
        this.url = url;

    }

    public abstract Connection getConnection();

    public <T extends OOPDatabase> T to(Class<T> to) {

        return to.cast(this);

    }

    public void cleanTable(String name) {

        run("DELETE FROM " + name);

    }

    public void createTable(String name, String... rows) {

        StringBuilder sqlBuilder = new StringBuilder("CREATE TABLE IF NOT EXISTS " + name + " (");

        int currentRow = 1;
        for (String row : rows) {

            if (currentRow < rows.length) sqlBuilder.append(row + ", ");
            else sqlBuilder.append(row + ")");
            currentRow++;

        }
        run(sqlBuilder.toString());

    }

    public String getUrl() {
        return url;
    }

    public SDatabaseType getType() {
        return type;
    }

    public void run(String sql) {

        try {

            Connection conn = getConnection();
            if (conn == null) return;
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public HashMap<Integer, HashMap<String, Object>> getAllRows(String tableName) {

        HashMap<Integer, HashMap<String, Object>> ret = new HashMap<>();
        Connection conn = getConnection();

        try {
            try (PreparedStatement selectStmt = conn.prepareStatement(
                    "SELECT * from " + tableName, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                 ResultSet rs = selectStmt.executeQuery()) {
                if (!rs.isBeforeFirst()) {
                } else {
                    while (rs.next()) {

                        int id = 0;
                        String firstColumn = null;
                        ResultSetMetaData metaData = rs.getMetaData();
                        int columnCount = metaData.getColumnCount();
                        boolean firstCountable = false;

                        for (int i = 1; i <= columnCount; i++) {
                            if (firstColumn == null) {

                                firstColumn = metaData.getColumnName(i);
                                Object object = rs.getObject(i);

                                if (object instanceof Integer) {
                                    firstCountable = true;
                                    id = (int) object;
                                }

                                HashMap<String, Object> data = new HashMap<>();
                                ret.put(id, data);
                                continue;

                            }

                            String columnName = metaData.getColumnName(i);
                            Object value = rs.getObject(i);

                            if (columnName.equalsIgnoreCase(firstColumn)) {

                                if(firstCountable)
                                    id = (int) value;
                                else
                                    id++;

                                HashMap<String, Object> data = new HashMap<>();
                                ret.put(id, data);

                            } else
                                ret.get(id).put(columnName, value);

                        }
                    }
                    rs.close();
                    selectStmt.close();
                    conn.close();
                }
            }
            conn.close();
        } catch (SQLException e) {
            return ret;
        }

        return ret;
    }

    public enum SDatabaseType {

        SQLITE,
        MYSQL

    }

}
