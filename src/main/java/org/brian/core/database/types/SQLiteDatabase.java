package org.brian.core.database.types;


import org.brian.core.database.OOPDatabase;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;

public class SQLiteDatabase extends OOPDatabase {

    private Connection lastConnection;

    public SQLiteDatabase(File dbParent, String dbName) {
        super("jdbc:sqlite:" + dbParent.getAbsolutePath() + File.separator + dbName + ".db", SDatabaseType.SQLITE);
    }

    @Override
    public Connection getConnection() {

        try {

            if (lastConnection == null || lastConnection.isClosed()) {

                Class.forName("org.sqlite.JDBC");
                lastConnection = DriverManager.getConnection(getUrl());

            } else {
                return lastConnection;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return lastConnection;

    }

    public void vaccum() {

        run("VACUUM");

    }

}
