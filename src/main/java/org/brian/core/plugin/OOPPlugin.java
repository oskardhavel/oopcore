package org.brian.core.plugin;

import org.brian.core.OOPCore;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public abstract class OOPPlugin extends JavaPlugin {

    private List<Runnable> disableRun = new ArrayList<>();
    private long resourceId;

    @Override
    public void onEnable() {

        enable();
        OOPCore.instance().hook(this);

    }

    @Override
    public void onDisable() {

        OOPCore.instance().unhook(this);
        disableRun.forEach(Runnable::run);
        disable();

    }

    public void onDisable(Runnable runnable) {
        this.disableRun.add(runnable);
    }

    public abstract void enable();

    public void disable() {
    }

    public void resourceId(long resourceId) {
        this.resourceId = resourceId;
    }
}
