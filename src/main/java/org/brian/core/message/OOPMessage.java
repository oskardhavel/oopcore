package org.brian.core.message;

import lombok.Getter;
import org.brian.core.message.additions.action.CommandAddition;
import org.brian.core.yaml.ConfigurationSection;
import org.brian.core.yaml.OOPConfiguration;
import org.bukkit.entity.Player;

import java.util.*;

@Getter
public class OOPMessage {

    private LinkedList<MessageLine> lineList = new LinkedList<>();
    private boolean center = false;

    public static OOPMessage fromSection(ConfigurationSection section) {

        OOPMessage message = new OOPMessage();
        if(section.isPresentValue("center"))
            message.center(section.valueAsRequired("center"));

        if (section.isPresentChild("lines")) {

            //Multiple Lines
            Map<Integer, MessageLine> messageLineMap = new HashMap<>();
            for (ConfigurationSection lineSection : section.child("lines").childs().values()) {

                int place = Integer.parseInt(lineSection.key());
                messageLineMap.put(place, initLine(lineSection));

            }
            messageLineMap.forEach((k, v) -> message.appendLine(v));

        } else
            message.appendLine(initLine(section));

        return message;

    }

    static MessageLine initLine(ConfigurationSection section) {

        MessageLine messageLine = new MessageLine();
        if (section.isPresentValue("text"))

            //Single Content = Single LineContent
            messageLine.append(initContent(section));

        else if (section.isPresentChild("content")) {

            Map<Integer, LineContent> lineContentMap = new HashMap<>();
            for (ConfigurationSection contentSection : section.child("content").childs().values()) {

                int place = Integer.parseInt(contentSection.key());
                lineContentMap.put(place, initContent(contentSection));

            }

            List<Integer> places = new ArrayList<>(lineContentMap.keySet());
            Collections.sort(places);

            places.forEach(place -> messageLine.append(lineContentMap.get(place)));

        } else {
            //Throw
        }

        return messageLine;

    }

    static LineContent initContent(ConfigurationSection section) {

        LineContent lineContent = new LineContent(section.valueAsRequired("text"));

        if (section.isPresentValue("hover")) lineContent.hoverText(section.valueAsRequired("hover"));
        if (section.isPresentValue("runCommand"))
            lineContent.addAddition(new CommandAddition().command(section.valueAsRequired("runCommand")));

        return lineContent;
    }

    public OOPMessage appendLine(MessageLine line) {
        this.lineList.add(line);
        return this;
    }

    public OOPMessage appendLine(String line) {
        this.lineList.add(new MessageLine().append(line));
        return this;
    }

    public OOPMessage center(boolean globalCenter) {
        this.center = globalCenter;
        return this;
    }

    public void send(Player player) {

        if (center) lineList.forEach(line -> line.center(center));
        lineList.forEach(line -> line.send(player));

    }

    public void send(Player player, Map<String, String> placeholders) {

        if (center) lineList.forEach(line -> line.center(center));
        lineList.forEach(line -> line.send(player, placeholders));

    }

    public void saveToConfig(OOPConfiguration configuration, String path) {

        if (lineList.size() == 1) {

            //We got one line
            MessageLine line = lineList.get(0);
            if (line.contentList().size() == 1) {
                LineContent lineContent = line.contentList().get(0);
                if (lineContent.getAdditionList().isEmpty() && lineContent.getHoverText() == null) {

                    //Simple set (as a value)
                    configuration.set(path, lineContent.getText());

                } else
                    saveLine(configuration.newSection(path), lineContent);
            } else {

                ConfigurationSection messageSection = configuration.newSection(path);
                if(center)
                    messageSection.set("center", true);
                ConfigurationSection contentSection = messageSection.newSection("content");
                int index = 1;
                for (LineContent lineContent : line.contentList()) {

                    ConfigurationSection textSection = contentSection.newSection(index + "");
                    saveLine(textSection, lineContent);
                    index++;

                }

            }

        } else if (lineList.size() > 1) {
            ConfigurationSection mainSection = configuration.newSection(path);
            ConfigurationSection linesSection = mainSection.newSection("lines");
            if(center)
                mainSection.set("center", true);

            int currentLine = 1;
            for (MessageLine line : lineList) {

                ConfigurationSection lineSection = linesSection.newSection(currentLine + "");

                if (line.contentList().size() == 1) {
                    LineContent lineContent = line.contentList().get(0);
                    if (lineContent.getAdditionList().isEmpty() && lineContent.getHoverText() == null) {

                        //Simple set (as a value)
                        lineSection.set("text", lineContent.getText());

                    } else
                        saveLine(lineSection, lineContent);
                } else {
                    ConfigurationSection contentSection = lineSection.newSection("content");
                    int index = 1;
                    for (LineContent lineContent : line.contentList()) {

                        ConfigurationSection textSection = contentSection.newSection(index + "");
                        saveLine(textSection, lineContent);
                        index++;

                    }
                }
                currentLine++;

            }
        }

    }

    private void saveLine(ConfigurationSection section, LineContent lineContent) {

        section.set("text", lineContent.getText());
        if (lineContent.getHoverText() != null)
            section.set("hover", lineContent.getHoverText());

        CommandAddition cmdAddition = lineContent.getAdditionList().stream().
                filter(o -> o instanceof CommandAddition).
                map(o -> (CommandAddition) o).
                findFirst().
                orElse(null);

        if (cmdAddition == null)
            section.set("runCommand", cmdAddition.command());

    }

    @Override
    public OOPMessage clone() {
        try {
            return ((OOPMessage) super.clone());
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}
