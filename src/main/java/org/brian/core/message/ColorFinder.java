package org.brian.core.message;


import org.apache.commons.lang.ArrayUtils;
import org.bukkit.ChatColor;

import java.util.Arrays;

public class ColorFinder {

    public String color;
    public String decoration;

    private ColorFinder(String color, String decoration) {
        this.color = color;
        this.decoration = decoration;
    }

    public String color() {
        return color;
    }

    public String decoration() {
        return decoration;
    }

    public static ColorFinder find(String input) {

        char[] charArray = input.toCharArray();
        String color = "";
        String decoration = "";

        for(Character c : charArray) {

            if(string(c).equalsIgnoreCase("&")) {

                int index = ArrayUtils.indexOf(charArray, c);
                if(arrayHas(index+1, charArray)) {

                    String secondChar = string(charArray[index+1]);
                    ChatColor chatColor = ChatColor.getByChar(secondChar.toCharArray()[0]);
                    if (chatColor == null) continue;

                    if(chatColor.isColor())
                        color = "&" + secondChar;
                    else
                        decoration = "&" + secondChar;

                }

            }

        }

        return new ColorFinder(color, decoration);

    }

    static String string(Object obj) {
        return obj.toString();
    }

    static boolean arrayHas(int index, char[] array) {

        return (array.length - 1) >= index;

    }


}
