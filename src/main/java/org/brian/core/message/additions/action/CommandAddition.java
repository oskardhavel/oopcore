package org.brian.core.message.additions.action;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.brian.core.message.additions.AAddition;

public class CommandAddition extends AAddition {

    private String command;

    @Override
    public void apply(TextComponent component) {

        if (command != null) {

            component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));

        }

    }

    public CommandAddition command(String command) {
        this.command = command;
        return this;
    }

    public String command() {
        return command;
    }

}
