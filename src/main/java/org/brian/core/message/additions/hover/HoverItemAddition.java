package org.brian.core.message.additions.hover;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.brian.core.OOPCore;
import org.brian.core.message.additions.AAddition;
import org.brian.core.utils.ItemStackUtil;
import org.bukkit.inventory.ItemStack;

public class HoverItemAddition extends AAddition {

    private ItemStack itemStack;

    @Override
    public void apply(TextComponent component) {

        if (itemStack != null) {
            try {

                BaseComponent[] components = new BaseComponent[]{new TextComponent(ItemStackUtil.itemStackToJson(itemStack))};
                component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_ITEM, components));

            } catch (Exception ex) {
                OOPCore.instance().logger().error(ex);
            }
        }

    }

    public HoverItemAddition itemStack(ItemStack item) {
        this.itemStack = item;
        return this;
    }

    @Override
    public HoverItemAddition clone() {

        HoverItemAddition hia = ((HoverItemAddition) super.clone());
        hia.itemStack(itemStack.clone());

        return hia;

    }
}
