package org.brian.core.message.additions.action;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.brian.core.OOPCore;
import org.brian.core.controller.PlayerController;
import org.brian.core.message.additions.AAddition;
import org.brian.core.player.OOPPLayer;
import org.brian.core.utils.Helper;
import org.bukkit.entity.Player;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class ConsumerAddition extends AAddition {

    private final long id;
    private long timeout = -1;
    private Consumer<Player> consumer;
    private boolean onetimeClick = false;
    private String timeoutMessage;

    public ConsumerAddition() {
        this.id = ThreadLocalRandom.current().nextLong(99999999);
    }

    public ConsumerAddition consumer(Consumer<Player> consumer) {
        this.consumer = consumer;
        return this;
    }

    public ConsumerAddition timeout(long timeout) {
        this.timeout = timeout;
        return this;
    }

    public ConsumerAddition timeout(int timeout, TimeUnit unit) {
        return timeout(unit.toMillis(timeout));
    }

    public ConsumerAddition timeoutMessage(String timeoutMessage) {
        this.timeoutMessage = timeoutMessage;
        return this;
    }

    public ConsumerAddition onetimeClick(boolean onetimeClick) {
        this.onetimeClick = onetimeClick;
        return this;
    }

    @Override
    public void apply(TextComponent component) {

        component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/oopCore:action:consumer:" + id));

    }

    @Override
    public void onSend(Player player) {

        OOPPLayer oopPlayer = PlayerController.instance().get(player);
        if (onetimeClick) {

            Consumer<Player> modifiedConsumer = p -> {

                consumer.accept(p);
                oopPlayer.unregisterConsumer(id);

            };
            oopPlayer.registerConsumer(id, modifiedConsumer);

        } else oopPlayer.registerConsumer(id, consumer);

        if (timeout > 0) {

            OOPCore.instance().energiser().scheduleRepeated(() -> {

                if (timeoutMessage != null) player.sendMessage(Helper.color(timeoutMessage));
                oopPlayer.unregisterConsumer(id);

            }, (int) timeout, TimeUnit.MILLISECONDS);

        }

    }

}
