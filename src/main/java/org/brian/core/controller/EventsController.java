package org.brian.core.controller;

import org.brian.core.OOPCore;
import org.brian.core.plugin.OOPPlugin;
import org.bukkit.event.HandlerList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventsController {

    private static EventsController instance;
    private Map<OOPPlugin, List<HandlerList>> registeredEvents = new HashMap<>();

    public EventsController() {
        if (instance == null) instance = this;
    }

    public static EventsController instance() {
        return instance;
    }

    public void unregister(OOPPlugin plugin) {
        if (registeredEvents.containsKey(plugin)) {

            registeredEvents.get(plugin).forEach(handler -> handler.unregister(OOPCore.instance()));
            registeredEvents.remove(plugin);

        }
    }

    public void register(OOPPlugin plugin, HandlerList handlerList) {

        if (registeredEvents.containsKey(plugin)) {

            registeredEvents.get(plugin).add(handlerList);

        } else {

            List<HandlerList> handlerLists = new ArrayList<>();
            handlerLists.add(handlerList);
            registeredEvents.put(plugin, handlerLists);

        }

    }

}
