package org.brian.core.controller;

import org.brian.core.player.OOPPLayer;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerController {

    private static PlayerController instance;
    private Map<UUID, OOPPLayer> playerMap = new HashMap<>();

    public PlayerController() {
        if (instance == null) instance = this;
    }

    public static PlayerController instance() {
        return instance;
    }

    public OOPPLayer get(UUID uuid) {

        if (playerMap.containsKey(uuid)) return playerMap.get(uuid);

        OOPPLayer player = new OOPPLayer(uuid);
        playerMap.put(uuid, player);


        return player;

    }

    public OOPPLayer get(Player player) {
        return get(player.getUniqueId());
    }

}
