package org.brian.core.controller;

import org.brian.core.energiser.ETask;
import org.brian.core.energiser.Energiser;
import org.brian.core.energiser.IScheduler;
import org.brian.core.plugin.OOPPlugin;
import org.bukkit.Bukkit;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class TaskController implements IScheduler {

    private Energiser energiser;
    private OOPPlugin owner;
    private Set<Long> ids = new HashSet<>();

    public TaskController(OOPPlugin plugin, Energiser supplier) {
        this.owner = plugin;
        this.energiser = supplier;
        plugin.onDisable(() -> {

            ids.forEach(id -> Bukkit.getScheduler().cancelTask(Integer.parseInt(id.toString())));

        });
    }

    @Override
    public ETask scheduleRepeated(Runnable runnable, long delay) {

        ETask task = energiser.scheduleRepeated(runnable, delay);
        if (task.isBukkitMode()) ids.add(task.id());

        return task;

    }

    @Override
    public void schedule(ETask eTask) {

        if (eTask.isBukkitMode()) ids.add(eTask.id());
        energiser.schedule(eTask);

    }

    @Override
    public void async(Runnable runnable, boolean pool) {
        energiser.async(runnable, pool);
    }


    public void sync(Runnable runnable) {
        energiser.sync(runnable);
    }

    public OOPPlugin owner() {
        return owner;
    }
}
