package org.brian.core.version;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;

public class VersionController {

    private static int version;
    private static String stringVersion;
    private static boolean init = false;

    static {

        if (!init) {
            init = true;
            String fullVersion = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
            stringVersion = StringUtils.replace(fullVersion, "_", " ");
            version = Integer.parseInt(StringUtils.replace(fullVersion.split("_")[1].split("_R")[0], "v", ""));
        }

    }

    public static boolean is(int version2) {
        return version == version2;
    }

    public static boolean isAfter(int version2) {
        return version > version2;
    }

    public static boolean isBefore(int version2) {
        return version < version2;
    }

    public static boolean isOrAfter(int version2) {
        return version == version2 || version > version2;
    }

    public String stringVersion() {
        return stringVersion;
    }

}
