package org.brian.core.yaml;

import org.apache.commons.math3.util.Pair;
import org.brian.core.file.OOPFile;
import org.brian.core.yaml.mapper.ObjectsMapper;
import org.brian.core.yaml.util.ConfigurationUtil;
import org.brian.core.yaml.util.OOPIterator;
import org.brian.core.yaml.util.UnreadString;
import org.brian.core.yaml.value.AConfigurationValue;
import org.brian.core.yaml.value.ConfigurationList;
import org.brian.core.yaml.value.ConfigurationValue;

import java.io.*;
import java.util.*;

import static java.util.stream.Collectors.toList;
import static org.brian.core.yaml.util.ConfigurationUtil.isValidIndex;

public class OOPConfiguration {

    private OOPFile oopFile;
    private Map<String, AConfigurationValue> values = new HashMap<>();
    private Map<String, ConfigurationSection> sections = new HashMap<>();
    private List<String> header = new ArrayList<>();

    public OOPConfiguration(File file) {
        try {

            this.oopFile = new OOPFile(file);

            BufferedReader reader = new BufferedReader(new FileReader(file));
            String sCurrentLine;
            int index = 0;
            List<UnreadString> lines = new ArrayList<>();

            boolean ignoring = false;
            while ((sCurrentLine = reader.readLine()) != null) {
                UnreadString unreadString = new UnreadString(index, sCurrentLine);
                if (unreadString.value().contains("#/*")) {
                    ignoring = true;
                    continue;
                } else if (unreadString.value().contains("#*/")) {
                    ignoring = false;
                    continue;
                }
                if (ignoring) continue;

                lines.add(unreadString);
                index++;

            }

            reader.close();
            UnreadString[] array = new UnreadString[lines.size()];
            lines.forEach(line -> array[line.index()] = line);

            ConfigurationSection key = null;

            OOPIterator<UnreadString> looper = new OOPIterator<>(array);
            List<UnreadString> mainSections = new ArrayList<>();

            List<String> description = new ArrayList<>();
            while (looper.hasNext()) {

                UnreadString line = looper.next();
                if (line == null) continue;

                if (ConfigurationUtil.firstCharsAfterSpaces(line.value(), 1).equalsIgnoreCase("#")) {

                    //We got a description
                    description.add(line.value());
                    continue;

                }

                if (line.value().contains(":")) {

                    String[] split = line.value().split(":");
                    if (split.length == 1) {

                        UnreadString[] valuesArray = looper.getObjectsCopy(UnreadString.class);
                        int elementIndex = Arrays.asList(valuesArray).indexOf(line);
                        boolean valid = isValidIndex(valuesArray, elementIndex + 1);

                        if (valid && ConfigurationUtil.isList(valuesArray, elementIndex + 1)) {

                            if (ConfigurationUtil.findSpaces(split[0]) >= 2) continue;

                            //Is list
                            List<UnreadString> listValues = looper.nextValuesThatMatches(us -> us.value().contains("-"), true);
                            Pair<String, Integer> parsedKey = ConfigurationUtil.parse(split[0]);
                            ConfigurationList value = new ConfigurationList(parsedKey.getKey(), listValues.stream().map(UnreadString::value).map(string -> ConfigurationUtil.parse(ConfigurationUtil.parse(string).getKey().substring(1)).getKey()).collect(toList()));

                            value.spaces(parsedKey.getValue());
                            value.description(description);
                            value.spaces(0);

                            values.put(value.key(), value);

                        } else {
                            if (ConfigurationUtil.findSpaces(split[0]) != 0) continue;
                            mainSections.add(line);
                        }

                    } else {

                        if (ConfigurationUtil.findSpaces(split[0]) >= 2) continue;

                        Pair<String, Integer> parsedKey = ConfigurationUtil.parse(split[0]);
                        ConfigurationValue value = new ConfigurationValue(parsedKey.getKey(), ObjectsMapper.mapObject(ConfigurationUtil.parse(split[1]).getKey()));

                        value.spaces(parsedKey.getValue());
                        value.description(description);
                        value.spaces(0);

                        values.put(value.key(), value);

                    }
                }

            }

            for (UnreadString headSection : mainSections) {

                int startingIndex = Arrays.asList(array).indexOf(headSection);
                int endIndex = ConfigurationUtil.findSectionEnd(startingIndex, looper);
                ConfigurationSection section = ConfigurationUtil.loadSection(new OOPIterator(ConfigurationUtil.copy(array, startingIndex, endIndex)));

                sections.put(section.key(), section);

            }

            if (key != null && key.parent() == null) sections.put(key.key(), key);
            new LineAssigner(this).assign();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public AConfigurationValue value(String path) {

        if (!path.contains(".")) {

            if (values.containsKey(path)) return values.get(path);
            else return null;

        } else {

            String[] split = path.split("\\.");
            String valueKey = split[split.length - 1];

            ConfigurationSection section = null;

            for (int index = 0; index < split.length - 1; index++) {

                String key = split[index];
                if (section == null) section = sections.get(key);
                else section = section.childs().get(key);

            }

            if (section != null && section.values().containsKey(valueKey)) return section.values().get(valueKey);

        }

        return null;

    }

    public <T> T value(String path, Class<T> type) {

        if (!path.contains(".")) {

            if (values.containsKey(path)) {

                if (type != null) return type.cast(values.get(path));
                else return (T) values.get(path);

            } else return null;

        } else {

            String[] split = path.split("\\.");
            String valueKey = split[split.length - 1];

            ConfigurationSection section = null;

            for (int index = 0; index < split.length - 1; index++) {

                String key = split[index];
                if (section == null) section = sections.get(key);
                else section = section.childs().get(key);

            }

            if (section != null && section.values().containsKey(valueKey))
                return section.values().get(valueKey).valueAsRequired();

        }

        return null;

    }

    public Pair<ConfigurationSection, List<ConfigurationSection>> section(String path) {

        List<ConfigurationSection> sections = new ArrayList<>();

        if (!path.contains(".")) {
            if (sections().containsKey(path)) sections.addAll(sections().get(path).childs().values());
            return new Pair<>(sections().get(path), sections);
        } else {

            String[] split = path.split("\\.");
            ConfigurationSection section = null;

            for (int index = 0; index < split.length; index++) {

                String key = split[index];
                if (section == null) section = sections().get(key);
                else section = section.childs().get(key);

            }

            if (section != null) {
                sections.addAll(section.allParents());
                return new Pair<>(section, sections);
            }
        }

        return null;

    }

    public <T> T valueAsRequired(String path) {
        return (T) value(path, null);
    }

    public AConfigurationValue set(String path, Object object) {

        AConfigurationValue value = null;
        if (object instanceof AConfigurationValue) {
            value = (AConfigurationValue) object;
        }

        if (!path.contains(".")) {
            if (value == null) value = AConfigurationValue.fromObject(path, object);
            values.put(path, value);
            return value;
        } else {

            String[] split = path.split("\\.");
            ConfigurationSection section = null;
            if (value == null) value = AConfigurationValue.fromObject(split[split.length - 1], object);
            int currentSpaces = 0;

            for (int index = 0; index < split.length - 1; index++) {

                String key = split[index];
                if (section == null) {
                    if (sections.containsKey(key)) {
                        section = sections.get(key);
                    } else {
                        section = new ConfigurationSection(key, currentSpaces);
                        sections.put(key, section);
                    }
                } else {
                    if (section.childs().containsKey(key)) section = section.childs().get(key);
                    else {
                        ConfigurationSection section2 = new ConfigurationSection(key, section.spaces());
                        section.assignChild(section2);
                        section = section2;
                    }
                }

                currentSpaces += 2;

            }

            if (section != null) {
                section.values().put(split[split.length - 1], value);
                value.spaces(value.spaces() <= 0 ? section.spaces() + 2 : value.spaces());
                value.parent(section);
            }
            return value;

        }
    }

    public Map<String, AConfigurationValue> values() {
        return values;
    }

    public Map<String, AConfigurationValue> allValues() {

        Map<String, AConfigurationValue> allValues = new HashMap<>();

        values.forEach((k, v) -> allValues.put(v.path(), v));
        for (ConfigurationSection section : sections.values()) {
            allValues.putAll(section.allValues());
        }

        return allValues;

    }

    public List<ConfigurationSection> allSections() {

        List<ConfigurationSection> sections = new ArrayList<>();
        sections().values().forEach(section -> sections.addAll(section.allChilds()));

        return sections;

    }

    public Map<String, ConfigurationSection> sections() {
        return sections;
    }

    public void save() {
        FileWriter w = null;
        BufferedWriter bw = null;

        try {

            oopFile.createIfNotExists();
            File file = oopFile.file();
            w = new FileWriter(file);
            bw = new BufferedWriter(w);

            bw.newLine();
            bw.write("#/*");
            bw.newLine();
            bw.write("#Configuration made with OOPCore!");
            bw.newLine();
            bw.write("#An awesome library written by OOP-778");
            bw.newLine();
            bw.write("#Support -> https://discord.gg/35fxvm6");
            bw.newLine();
            bw.write("#SpigotMC -> https://www.spigotmc.org/members/brian.562713/");
            bw.newLine();
            bw.write("#GitLab -> https://gitlab.com/oskardhavel");
            bw.newLine();
            bw.write("#---------------------");
            bw.newLine();
            bw.newLine();
            for (String head : header) {
                bw.write("#" + head);
                bw.newLine();
            }
            bw.write("#*/");
            bw.newLine();

            for (AConfigurationValue value : values().values()) {

                bw.newLine();
                value.writeDescription(bw, value.spaces());
                value.write(bw);

            }

            for (ConfigurationSection section : sections.values()) {

                bw.newLine();
                section.writeDescription(bw, section.spaces());
                section.write(bw);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (bw != null) bw.close();
                if (w != null) w.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    public File file() {
        return oopFile.file();
    }

    public OOPConfiguration file(File file) {
        this.oopFile = new OOPFile(file);
        return this;
    }

    public boolean isPresentValue(String path) {
        return value(path, null) != null;
    }

    public boolean isPresentChild(String child) {
        return sections().containsKey(child);
    }

    public ConfigurationSection newSection(String path) {

        if (!path.contains(".")) {

            ConfigurationSection section = new ConfigurationSection(path, 0);
            sections.put(path, section);
            return section;

        } else {
            String[] split = path.split("\\.");
            ConfigurationSection parent = null;

            for (int index = 0; index < split.length - 1; index++) {

                String key = split[index];
                if (parent == null) parent = sections().get(key);
                else parent = parent.childs().get(key);

            }

            if (parent != null) {
                String sectionName = split[split.length - 1];
                int spaces = parent.spaces() + 2;
                ConfigurationSection section = new ConfigurationSection(sectionName, spaces);
                parent.assignChild(section);
                return section;
            }

        }

        return null;
    }

    public void appendHeader(String string) {
        this.header.add(string);
    }

}
