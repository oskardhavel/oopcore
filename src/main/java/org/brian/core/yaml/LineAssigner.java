package org.brian.core.yaml;

import lombok.Getter;
import org.brian.core.logger.OOPLogger;
import org.brian.core.yaml.util.ConfigurationUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LineAssigner {

    private OOPConfiguration configuration;

    public LineAssigner(OOPConfiguration configuration) {
        this.configuration = configuration;
    }

    public void assign() {

        //Load all the lines
        File configurationFile = configuration.file();
        if (!configurationFile.exists()) return;

        List<Line> lineList = new ArrayList<>();
        OOPLogger logger = OOPLogger.builder().
                initializer(LineAssigner.class).
                build().name("YAML");

        try {
            BufferedReader reader = new BufferedReader(new FileReader(configurationFile));
            String sCurrentLine;
            int line = 1;

            while ((sCurrentLine = reader.readLine()) != null) {

                if (ConfigurationUtil.containsChar(sCurrentLine) && !String.valueOf(sCurrentLine.trim().charAt(0)).equalsIgnoreCase("#")) {
                    sCurrentLine = sCurrentLine.split(":")[0];
                    lineList.add(new Line(sCurrentLine, line));
                }
                line++;

            }

            reader.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //Start off with assigning lines for sections
        configuration.allSections().forEach(section -> {

            Optional<Line> fileLine = lineList.stream().
                    filter(fl -> fl.content.contains(section.key())).
                    findFirst();

            if (!fileLine.isPresent()) {
                System.out.println("Line of section" + section.key() + " is not found!");
            } else {

                section.line(fileLine.get().line);
                lineList.remove(fileLine.get());

            }

        });


        //Then assign values lines
        configuration.allValues().values().forEach(value -> {

            Optional<Line> fileLine = lineList.stream().
                    filter(fl -> fl.content.contains(value.key())).
                    findFirst();

            if (!fileLine.isPresent()) {
                System.out.println("Line of value " + value.key() + " cannot be found!");
            } else {

                lineList.remove(fileLine.get());
                value.line(fileLine.get().line);

            }

        });

    }

    @Getter
    private class Line {

        private String content;
        private int line;

        public Line(String content, int line) {
            this.content = content;
            this.line = line;
        }


    }

}
