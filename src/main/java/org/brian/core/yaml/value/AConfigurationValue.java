package org.brian.core.yaml.value;

import org.brian.core.yaml.ConfigurationSection;
import org.brian.core.yaml.util.Descriptionable;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class AConfigurationValue extends Descriptionable {

    private String key;
    private ConfigurationSection parent;
    private int spaces = 0;
    private int line = -1;

    public AConfigurationValue(String key, ConfigurationSection parent) {
        this.key = key;
        this.parent = parent;
    }

    public AConfigurationValue(String key) {
        this(key, null);
    }

    public static AConfigurationValue fromObject(String key, Object obj) {

        AConfigurationValue value;

        if (obj instanceof ArrayList)
            value = new ConfigurationList(key, (List<Object>) obj);
        else
            value = new ConfigurationValue(key, obj);

        return value;

    }

    public int spaces() {
        return spaces;
    }

    public AConfigurationValue spaces(int spaces) {
        this.spaces = spaces;
        return this;
    }

    public ConfigurationSection parent() {
        return parent;
    }

    public AConfigurationValue parent(ConfigurationSection section) {
        this.parent = section;
        return this;
    }

    public String path() {
        return parent == null ? key() : parent.key() + "." + key();
    }

    public String key() {
        return key;
    }

    public abstract Object value();

    public <T> T valueAsRequired() {
        return (T) value();
    }

    public abstract void write(BufferedWriter bw) throws IOException;

    public int line() {
        return line;
    }

    public AConfigurationValue line(int line) {
        this.line = line;
        return this;
    }

}
