package org.brian.core.yaml.value;

import org.brian.core.yaml.mapper.ObjectsMapper;
import org.brian.core.yaml.util.ConfigurationUtil;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

public class ConfigurationList extends AConfigurationValue {

    private List<Object> values;

    public ConfigurationList(String key, List<Object> values) {
        super(key, null);
        this.values = values;
    }

    @Override
    public Object value() {
        return values;
    }


    @Override
    public void write(BufferedWriter bw) throws IOException {

        bw.write(ConfigurationUtil.stringWithSpaces(spaces()) + key() + ":");
        for (Object value : values) {

            bw.newLine();
            bw.write(ConfigurationUtil.stringWithSpaces(spaces()) + "- " + ObjectsMapper.toString(value));

        }

    }
}
