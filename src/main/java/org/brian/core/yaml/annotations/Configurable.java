package org.brian.core.yaml.annotations;

import org.brian.core.yaml.ConfigurationSection;
import org.brian.core.yaml.OOPConfiguration;
import org.brian.core.yaml.mapper.SectionMappers;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

public interface Configurable {

    default void initConfig(OOPConfiguration configuration) throws ClassNotFoundException, IllegalAccessException, InstantiationException {

        for (Field field : getClass().getDeclaredFields()) {

            field.setAccessible(true);

            for (Annotation annotation : field.getDeclaredAnnotations()) {

                Class reqProductType = null;

                if (annotation instanceof ConfigValue) {

                    ConfigValue configValue = (ConfigValue) annotation;

                    //Check if value exists
                    if (!configuration.isPresentValue(configValue.path()))
                        throw new IllegalStateException("Failed to find configuration value at (" + configValue.path() + ") file (" + configuration.file().getName() + ")");

                    reqProductType = field.getType();
                    Object value = configuration.value(configValue.path()).value();

                    //Check if configuration value is parsable to field type
                    if (value.getClass().isInstance(reqProductType))
                        throw new IllegalStateException("Failed to parse configuration value at (" + configValue.path() + ") file (" + configuration.file().getName() + ") as " + reqProductType.getTypeName());

                    //Set the field
                    field.set(this, value);

                } else if (annotation instanceof SectionValue) {

                    SectionValue sectionValue = (SectionValue) annotation;

                    //We have to find out if the section at given path even exists
                    if (!configuration.isPresentChild(sectionValue.path()))
                        throw new IllegalStateException("Failed to find configuration section at (" + sectionValue.path() + ") file (" + configuration.file().getName() + ")");

                    //Requires a list of values, else it is a single value
                    if (field.getGenericType() instanceof ParameterizedType) {

                        //Check if field value is initialized
                        if (field.get(this) == null)
                            throw new IllegalStateException("The field list should be initialized in order for configuration to init!");

                        ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
                        for (Type t : parameterizedType.getActualTypeArguments()) {

                            reqProductType = Class.forName(t.getTypeName());
                            break;

                        }

                        if (reqProductType == null) continue;

                        //Because requirement class is ready to be used
                        //It's time to look for section mapper for required class
                        if (!SectionMappers.isMapperPresent(reqProductType))
                            throw new IllegalStateException("Failed to find Section Mapper for class " + reqProductType.getTypeName());

                        //Great we've got the mapper, now we have to find child sections of the parent one.
                        //if no childs are found, or some failed to build, we will provide empty list (null safety)
                        ConfigurationSection section = configuration.section(sectionValue.path()).getFirst();

                        List list = (List) field.get(this);

                        for (ConfigurationSection childSection : section.childs().values()) {

                            Object mappedObject = SectionMappers.map(childSection, reqProductType);
                            list.add(mappedObject);

                        }

                    } else {

                        reqProductType = field.getType();
                        if (reqProductType == null) continue;

                        //Because requirement class is ready to be used
                        //It's time to look for section mapper for required class
                        if (!SectionMappers.isMapperPresent(reqProductType))
                            throw new IllegalStateException("Failed to find Section Mapper for class " + reqProductType.getTypeName());

                        //Great we've got the mapper, now we have to find child sections of the parent one.
                        //if no childs are found, or some failed to build, we will provide empty list (null safety)
                        ConfigurationSection section = configuration.section(sectionValue.path()).getFirst();

                        Object mappedObject = SectionMappers.map(section, reqProductType);

                        //Set the field
                        field.set(this, mappedObject);

                    }

                }

            }

        }

    }

}
