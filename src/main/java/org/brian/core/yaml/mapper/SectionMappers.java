package org.brian.core.yaml.mapper;

import org.brian.core.yaml.ConfigurationSection;
import org.brian.core.yaml.mapper.section.SectionMapper;

import java.util.HashMap;
import java.util.Map;

public class SectionMappers {

    private static Map<Class, SectionMapper> mappers = new HashMap<>();

    public static <T> T map(ConfigurationSection section, Class<T> to) {

        if (!mappers.containsKey(to)) return null;

        SectionMapper mapper = mappers.get(to);
        if (!mapper.accepts(section)) return null;

        return (T) mapper.map(section);

    }

    public static void register(SectionMapper mapper) {
        if (mappers.containsKey(mapper.productClass)) return;
        mappers.put(mapper.productClass, mapper);
    }

    public static boolean isMapperPresent(Class clazz) {
        return mappers.containsKey(clazz);
    }

}
