package org.brian.core.yaml.mapper.section;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class MapRequirement {

    public String path;
    public String error_message;
    public Class type;

}
