package org.brian.core.yaml.mapper.section;

import org.brian.core.yaml.ConfigurationSection;
import org.brian.core.yaml.mapper.SectionMappers;

import java.util.ArrayList;
import java.util.List;

public abstract class SectionMapper<T> {

    public List<MapRequirement> requirementList = new ArrayList<>();
    public Class<T> productClass;

    public SectionMapper(Class<T> productClass) {
        this.productClass = productClass;
    }

    public boolean accepts(ConfigurationSection section) throws IllegalArgumentException {

        for (MapRequirement req : requirementList) {

            //Test Number 1
            if (!section.isPresentValue(req.getPath())) {
                throw new IllegalArgumentException(req.getError_message());
            }

            //Test Number 2
            if (req.type != null && req.type.isInstance(section.value(req.path)))
                throw new IllegalArgumentException(req.getError_message());

        }

        return true;

    }

    public void register() {
        SectionMappers.register(this);
    }

    public SectionMapper<T> addRequirement(MapRequirement req) {
        this.requirementList.add(req);
        return this;
    }

    public abstract T map(ConfigurationSection section);

}
