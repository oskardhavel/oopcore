package org.brian.core.file;

import org.brian.core.OOPCore;
import org.brian.core.plugin.OOPPlugin;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class OOPFile {

    private String fileName;
    private File folder;
    private File file;

    public OOPFile(File folder, String fileName) {
        this.fileName = fileName;
        this.folder = folder;
    }

    public OOPFile(String fileName) {
        this.fileName = fileName;
    }

    public OOPFile(File file) {
        this.file = file;
        this.fileName = file.getName();
    }

    public void createIfNotExists() {
        createIfNotExists(false, null);
    }

    public void rename(String to) {
        this.fileName = to;
        File dest = new File(file.getPath().replace(file.getName(), to));

        try {
            if (file != null) {
                Files.move(Paths.get(file.getPath()), Paths.get(dest.getPath()));
                this.file = new File(dest.getPath());
            } else file = folder != null ? new File(folder, fileName) : new File(fileName);
        } catch (Exception ex) {
            OOPCore.instance().logger().error(ex);
        }
    }

    public File file() {
        return file != null ? file : folder != null ? new File(folder, fileName) : new File(fileName);
    }

    public void createIfNotExists(boolean importFromResources, OOPPlugin plugin) {

        try {
            File file = folder == null ? new File(fileName) : new File(folder, fileName);
            if (!file.exists()) {

                if (importFromResources || plugin != null) plugin.saveResource(fileName, true);
                else file.createNewFile();

            }
        } catch (Exception ex) {
            OOPCore.instance().logger().error(ex);
        }

    }

}
